#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use tauri::{generate_handler, AppHandle};

#[tauri::command]
fn quit_app(app: AppHandle) -> Result<(), ()> {
    app.exit(0);

    Ok(())
}

#[tauri::command]
fn hello(name: &str) -> Result<String, String> {
    if name.contains(' ') {
        Err("Name should not contain spaces".to_owned())
    } else {
        Ok(format!("Hello, {name}"))
    }
}

pub fn run() {
    tauri::Builder::default()
        .invoke_handler(generate_handler![hello, quit_app])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
