use wasm_bindgen::{prelude::wasm_bindgen, JsCast, JsValue};
use wasm_bindgen_futures::spawn_local;
use web_sys::{window, HtmlInputElement};
use yew::{
    function_component, html, use_effect_with_deps, use_state_eq, Callback, Html, UseStateHandle,
};

#[wasm_bindgen(module = "/src/glue.js")]
extern "C" {
    #[wasm_bindgen(js_name = invokeHello, catch)]
    pub async fn hello(name: String) -> Result<JsValue, JsValue>;

    #[wasm_bindgen(js_name = quit_app, catch)]
    pub async fn quit_app() -> Result<(), JsValue>;
}

#[function_component]
fn App() -> Html {
    let welcome = use_state_eq(|| "".to_owned());
    let name = use_state_eq(|| "Tauri/Yew".to_owned());

    {
        let welcome = welcome.clone();

        use_effect_with_deps(
            move |name| {
                update_welcome_message(welcome, name.clone());

                || ()
            },
            (*name).clone(),
        );
    }

    let close_button_onclick = Callback::from(|_| {
        spawn_local(async move {
            match quit_app().await {
                Ok(_) => {}
                Err(e) => {
                    let window = window().unwrap();

                    window
                        .alert_with_message(&format!("Error: {}", e.as_string().unwrap()))
                        .unwrap();
                }
            }
        });
    });

    let name_onchange = Callback::from(move |event: yew::Event| {
        let value = event
            .target()
            .unwrap()
            .unchecked_ref::<HtmlInputElement>()
            .value();

        name.set(value);
    });

    let message = (*welcome).clone();

    html! {
        <div>
            <h1 class="heading">{message}</h1>
            <hr />
            <label>{"New Name: "}<input type="text" onchange={name_onchange} /></label>
            <hr />
            <button type="button" onclick={close_button_onclick}>{"Exit"}</button>
        </div>
    }
}

fn update_welcome_message(welcome: UseStateHandle<String>, name: String) {
    spawn_local(async move {
        match hello(name).await {
            Ok(message) => {
                welcome.set(message.as_string().unwrap());
            }
            Err(e) => {
                let window = window().unwrap();

                window
                    .alert_with_message(&format!("Error: {}", e.as_string().unwrap()))
                    .unwrap();
            }
        }
    });
}

fn main() {
    yew::Renderer::<App>::new().render();
}
