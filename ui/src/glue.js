
const { invoke } = window.__TAURI__.core

export async function invokeHello(name) {
  return await invoke('hello', { name })
}

export async function quit_app() {
  return await invoke('quit_app')
}
