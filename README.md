# Tauri / Yew Demo

Example project based on article [Create a desktop app in Rust using Tauri and Yew](https://dev.to/stevepryde/create-a-desktop-app-in-rust-using-tauri-and-yew-2bhe) by [Steve Pryde](https://dev.to/stevepryde).

- [Steve Pryde's project](https://github.com/stevepryde/tauri-yew-demo/tree/main/src-tauri)

## License

Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>

This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it and/or modify it
under the terms of the BSD 2-clause License. See the LICENSE file for more
details.
